function getNeighbors(map, xcord, ycord) {
  if (typeof map[xcord] === 'undefined' || typeof map[xcord][ycord] === 'undefined') {
    throw new Error('outside of map\'s range');
  }
  if (map[xcord][ycord] === '#') {
    throw new Error('you have stepped inside wall');
  }

  const rowLength = map[0].length;
  const neighbors = [];
  for (let j = ycord - 1; j <= ycord + 1; j += 1) {
    if (j >= 0 && j < rowLength) {
      for (let i = xcord - 1; i <= ycord + 1; i += 1) {
        if (i >= 0 && i < rowLength) {
          const char = map[i][j];
          if (char !== '#' && !(i === xcord && j === ycord)) {
            neighbors.push({
              x: i,
              y: j,
              type: getType(char),
            });
          }
        }
      }
    }
  }
  return neighbors;

  function getType(char) {
    switch (char) {
      case 'O': return 'start';
      case 'E': return 'end';
      default: return 'space';
    }
  }
}

export default getNeighbors;
