function contains(visited, x, y) {
  const found = visited.filter((item) => item[0] === x && item[1] === y);
  return !!(found && found.length);
}

export default contains;
