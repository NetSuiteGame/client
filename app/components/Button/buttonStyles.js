import { css } from 'styled-components';

const buttonStyles = css`
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  border-radius: 4px;
  border: 2px solid #41addd;
  box-sizing: border-box;
  color: #41addd;
  cursor: pointer;
  display: inline-block;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-size: 16px;
  font-weight: bold;
  outline: 0;
  padding: 0.25em 2em;
  text-decoration: none;
  user-select: none;

  &:active {
    background: #41addd;
    color: #fff;
  }
`;

export default buttonStyles;
