import styled from 'styled-components';

const FullHeight = styled.div`
  display: flex;
  flex-direction: ${(props) => props.row ? 'row' : 'column'};
  height: 100vh;
`;

export default FullHeight;
