import styled from 'styled-components';

const Row = styled.div`
  align-content: ${(props) => props.alignContent ? props.alignContent : 'flex-start'};
  display: flex;
  flex-direction: row;
`;

export default Row;
