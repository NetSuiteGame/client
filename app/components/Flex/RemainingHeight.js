import styled from 'styled-components';

const RemainingHeight = styled.div`
  flex: 1;
`;

export default RemainingHeight;
