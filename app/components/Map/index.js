/**
*
* Map
*
*/

import React, { PropTypes } from 'react';
import { RemainingHeight } from '../Flex';
// import styled from 'styled-components';


class Map extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.data = this.props.map.trim().split('\n');
  }

  componentDidMount() {
    const ctx = this.canvas.getContext('2d');
    ctx.fillRect(0, 0, 100, 100);
  }

  render() {
    console.log('data', this.data);
    return (
      <RemainingHeight>
        <canvas ref={(c) => { this.canvas = c; }} width={300} height={300} />
      </RemainingHeight>
    );
  }
}

Map.propTypes = {
  map: PropTypes.string,
};

export default Map;
