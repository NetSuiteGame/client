/**
*
* Pixi
*
*/

import React from 'react';
import { RemainingHeight } from '../Flex';

function Pixi() {
  return (
    <RemainingHeight>
      WebGL
    </RemainingHeight>
  );
}

Pixi.propTypes = {

};

export default Pixi;
