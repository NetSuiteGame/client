import styled from 'styled-components';

const Wrapper = styled.div`
  position: absolute;
  right: 15px;
  top: 15px;
  display: flex;
  flex-direction: row;
`;

export default Wrapper;
