import Link from '../Link';
import styled from 'styled-components';

export default styled(Link)`
  font-size: 16px;
  margin: 0 0 0 1em;
  padding: 0.25em 1em;

  &:active {
    background: #41ADDD;
    color: #FFF;
  }
`;
