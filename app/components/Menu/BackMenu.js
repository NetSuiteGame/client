import Img from '../Img';
import Link from './Link';
import React, { PropTypes } from 'react';
import Wrapper from './Wrapper';

/* eslint-disable global-require */
class BackMenu extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Wrapper>
        <Link to={this.props.to}><Img src={require('./assets/home.png')} alt="Back" /></Link>
      </Wrapper>
    );
  }
}

BackMenu.propTypes = {
  to: PropTypes.string,
};

export default BackMenu;
