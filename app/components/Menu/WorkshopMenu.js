/**
*
* WorkshopMenu
*
*/

import Img from '../Img';
import Link from './Link';
import React, { PropTypes } from 'react';
import Wrapper from './Wrapper';

/* eslint-disable global-require */
class WorkshopMenu extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Wrapper>
        <Link to="/workshop"><Img src={require('./assets/workshop.png')} alt="Workshop" /></Link>
        <Link to={this.props.helpRoute}><Img src={require('./assets/help.png')} alt="Help" /></Link>
      </Wrapper>
    );
  }
}

WorkshopMenu.propTypes = {
  helpRoute: PropTypes.string,
};

export default WorkshopMenu;
