import styled from 'styled-components';

const Content = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 16px;
`;

export default Content;
