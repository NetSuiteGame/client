import { Link } from 'react-router';
import styled from 'styled-components';

export default styled(Link)`
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  border-radius: 4px;
  border: 2px solid #41ADDD;
  color: #41ADDD;
  cursor: pointer;
  display: inline-flex;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin: 1em;
  justify-content: center;
  outline: 0;
  padding: 0.25em 2em;
  text-decoration: none;
  user-select: none;

  &:active {
    background: #41ADDD;
    color: #FFF;
  }
`;
