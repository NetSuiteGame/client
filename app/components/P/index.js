import styled from 'styled-components';

const P = styled.p`
  font-size: 1em;
  margin-bottom: 0.25em;
`;

export default P;
