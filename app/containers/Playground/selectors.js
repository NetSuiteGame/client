import { createSelector } from 'reselect';

/**
 * Direct selector to the playground state domain
 */
const selectPlaygroundDomain = () => (state) => state.get('playground');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Playground
 */

const makeSelectPlayground = () => createSelector(
  selectPlaygroundDomain(),
  (substate) => substate.toJS()
);

export default makeSelectPlayground;
export {
  selectPlaygroundDomain,
};
