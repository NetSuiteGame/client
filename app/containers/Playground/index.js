/*
 *
 * Playground
 *
 */

import Content from '../../components/Content';
import H1 from '../../components/H1';
import Helmet from 'react-helmet';
import React, { PropTypes } from 'react';
import makeSelectPlayground from './selectors';
import { BackMenu } from '../../components/Menu';
import { connect } from 'react-redux';
import { FullHeight } from '../../components/Flex';
import { createStructuredSelector } from 'reselect';
import Pixi from '../../components/Pixi';

export class Playground extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Content>
        <Helmet
          title="Playground"
          meta={[
            { name: 'description', content: 'Description of Playground' },
          ]}
        />
        <BackMenu to="/" />
        <FullHeight>
          <H1>Playground</H1>
          <Pixi />
        </FullHeight>
      </Content>
    );
  }
}

Playground.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  playground: makeSelectPlayground(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Playground);
