/*
 *
 * Warzone
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import makeSelectWarzone from './selectors';
import FullHeight from '../../components/Flex/FullHeight';

export class Warzone extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <FullHeight>
        <Helmet
          title="Warzone"
          meta={[
            { name: 'description', content: 'Description of Warzone' },
          ]}
        />
      </FullHeight>
    );
  }
}

Warzone.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Warzone: makeSelectWarzone(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Warzone);
