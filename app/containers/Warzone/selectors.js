import { createSelector } from 'reselect';

/**
 * Direct selector to the warzone state domain
 */
const selectWarzoneDomain = () => (state) => state.get('warzone');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Warzone
 */

const makeSelectWarzone = () => createSelector(
  selectWarzoneDomain(),
  (substate) => substate.toJS()
);

export default makeSelectWarzone;
export {
  selectWarzoneDomain,
};
