/*
 *
 * Workshop
 *
 */

import Content from '../../components/Content';
import H1 from '../../components/H1';
import Helmet from 'react-helmet';
import React, { PropTypes } from 'react';
import makeSelectWorkshop from './selectors';
import { BackMenu } from '../../components/Menu';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { FullHeight } from '../../components/Flex';
import smallMap from './map';
import Map from '../../components/Map';

export class Workshop extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Content>
        <Helmet
          title="NetSuite Game - Home Page"
          meta={[
            { name: 'description', content: 'NetSuite - Game' },
          ]}
        />
        <BackMenu to="/" />
        <FullHeight>
          <H1>Playground</H1>
          <Map map={smallMap} />
        </FullHeight>
      </Content>
    );
  }
}

Workshop.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Workshop: makeSelectWorkshop(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Workshop);
