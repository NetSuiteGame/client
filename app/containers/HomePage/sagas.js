// /**
//  * Handle dialog actions
//  */
//
import { put, takeEvery, select, call } from 'redux-saga/effects';
import { START_GAME, START_GAME_SUCCESS, START_GAME_FAILED } from './constants';
import request from 'utils/request';
import { makeSelectCurrentUser } from '../App/selectors';
import { showErrorMessage } from '../App/actions';

const requestUrl = '/crud/startgame';

export function* watchDialog() {
  yield takeEvery(START_GAME, dialogSaga);
}

export function* dialogSaga() {
  const username = yield select(makeSelectCurrentUser());
  try {
    yield call(request, requestUrl, {
      method: 'POST',
      data: username,
    });
    put({ type: START_GAME_SUCCESS });
  } catch (e) {
    yield put({ type: START_GAME_FAILED });
    yield put(showErrorMessage('Starting game failers '));
  }
}

export default [
  watchDialog,
];
