/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const SHOW_DIALOG = 'netsuitegame/Home/SHOW_DIALOG';
export const HIDE_DIALOG = 'netsuitegame/Home/HIDE_DIALOG';
export const START_GAME = 'netsuitegame/Home/START_GAME';
export const START_GAME_SUCCESS = 'netsuitegame/Home/START_GAME_SUCCESS';
export const START_GAME_FAILED = 'netsuitegame/Home/START_GAME_FAILED';
export const START_LOADING = 'netsuitegame/Home/START_LOADING';
export const STOP_LOADING = 'netsuitegame/Home/STOP_LOADING';
export const DIALOG_ERROR = 'netsuitegame/Home/DIALOG_ERROR';
