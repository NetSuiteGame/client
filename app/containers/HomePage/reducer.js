/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import {
  HIDE_DIALOG,
  SHOW_DIALOG,
  START_GAME_FAILED,
  START_GAME_SUCCESS,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  dialog: {
    state: 'closed',
    loading: false,
  },
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_DIALOG:
      return state
        .setIn(['dialog', 'state'], 'open');
    case START_GAME_SUCCESS:
    case START_GAME_FAILED:
    case HIDE_DIALOG:
      return state
        .setIn(['dialog', 'state'], 'closed');
    default:
      return state;
  }
}

export default homeReducer;
