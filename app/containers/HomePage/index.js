/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import CenteredContent from '../../components/Flex/CenteredContent';
import Column from '../../components/Flex/Column';
import Content from '../../components/Content';
import Helmet from 'react-helmet';
import Link from '../../components/Link';
import React, { PropTypes, PureComponent } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectDialogState, makeSelectDialogLoading } from './selectors';
import { showDialog, hideDialog, startGame } from './actions';
import WarzoneDialog from './WarzoneDialog';

export class HomePage extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      dialogLoading,
      dialogState,
      dispatchStartGame,
      hideWarzoneDialog,
      showWarzoneDialog,
    } = this.props;
    return (
      <Content>
        <Helmet
          title="NetSuite Game - Home Page"
          meta={[
            { name: 'description', content: 'NetSuite - Game' },
          ]}
        />
        <WarzoneDialog
          handleCancel={hideWarzoneDialog}
          handleConfirm={dispatchStartGame}
          show={dialogState === 'open'}
          loading={dialogLoading}
        />
        <CenteredContent>
          <Column>
            <Link to="/workshop">Workshop</Link>
            {/* <Link to="/playground">Playground</Link> */}
            <Link onClick={showWarzoneDialog}>Warzone</Link>
          </Column>
        </CenteredContent>
      </Content>
    );
  }
}

HomePage.propTypes = {
  dialogLoading: PropTypes.bool.isRequired,
  dialogState: PropTypes.string.isRequired,
  dispatchStartGame: PropTypes.func.isRequired,
  hideWarzoneDialog: PropTypes.func.isRequired,
  showWarzoneDialog: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  dialogState: makeSelectDialogState(),
  dialogLoading: makeSelectDialogLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    showWarzoneDialog: () => dispatch(showDialog()),
    hideWarzoneDialog: () => dispatch(hideDialog()),
    dispatchStartGame: () => dispatch(startGame()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
