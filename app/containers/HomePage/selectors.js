/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectHome = (state) => state.get('home');

const makeSelectDialogState = () => createSelector(
  selectHome,
  (homeState) => homeState.getIn(['dialog', 'state'])
);

const makeSelectDialogLoading = () => createSelector(
  selectHome,
  (homeState) => homeState.getIn(['dialog', 'loading'])
);

export {
  selectHome,
  makeSelectDialogState,
  makeSelectDialogLoading,
};
