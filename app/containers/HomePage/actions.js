/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  SHOW_DIALOG,
  HIDE_DIALOG,
  START_GAME,
  // DIALOG_ERROR,
} from './constants';

/**
 * Opens modal dialog
 *
 * @return {object}    An action object with a type of SHOW_DIALOG
 */
export function showDialog() {
  return {
    type: SHOW_DIALOG,
  };
}

/**
 * Closes modal dialog
 *
 * @return {object}    An action object with a type of HIDE_DIALOG
 */
export function hideDialog() {
  return {
    type: HIDE_DIALOG,
  };
}

/**
 * Starts saga
 *
 * @return {object}    An action object with a type of START_GAME
 */
export function startGame() {
  return {
    type: START_GAME,
  };
}
