/**
*
* Login screen in case user is for the first time
*
*/

import H2 from '../../components/H2';
import LoadingIndicator from '../../components/LoadingIndicator';
import P from '../../components/P';
import React, { PropTypes } from 'react';
import styled from 'styled-components';
import buttonStyles from '../../components/Button/buttonStyles';

const Overlay = styled.div`
  background-color: rgba(0, 0, 0, .2);
  height: 100vh;
  left: 0;
  position: absolute;
  top: 0;
  width: 100vw;
  z-index: 1;
`;

const Window = styled.div`
  background: #fff;
  margin: 0 auto;
  padding: 15px;
  position: relative;
  text-align: center;
  top: 50%;
  transform: translateY(-50%);
  width: 500px;
`;

const Heading = styled(H2)`
  padding-bottom: 0.25em;
`;

const Button = styled.button`${buttonStyles}`;

const ConfirmButton = styled(Button)`
  /*display: inline-flex;*/
  margin: 1em 2.5em;
`;

const CancelButton = styled(ConfirmButton)`
  border: 2px solid #c90909;
  color: #c90909;
`;

const ButtonWrapper = styled.div`
  text-align: center;
`;

function WarzoneDialog(props) {
  const { show, loading, handleConfirm, handleCancel } = props;
  if (show) {
    return (
      <Overlay>
        <Window>
          <Heading>Start game</Heading>
          <P>This will start the game. Are you sure?</P>

          {loading
            ? <LoadingIndicator />
            : (
              <ButtonWrapper>
                <ConfirmButton onClick={handleConfirm}>Save</ConfirmButton>
                <CancelButton onClick={handleCancel}>Cancel</CancelButton>
              </ButtonWrapper>
            )}
        </Window>
      </Overlay>
    );
  }
  return null;
}

WarzoneDialog.propTypes = {
  show: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  handleConfirm: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
};

export default WarzoneDialog;
