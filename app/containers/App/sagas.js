import { take, takeEvery, put, call, select } from 'redux-saga/effects';
import {
  LOAD_USER,
  LOAD_USER_FAIL,
  USER_DIALOG_SUBMIT,
  USER_DIALOG_SET_LOADING,
} from './constants';
import { showUserDialog, hideUserDialog, loadUserSuccess, showErrorMessage } from './actions';
import { makeSelectInputUserDialog } from './selectors';
import request from 'utils/request';

const requestUrl = '/crud/saveuser';

function loadUser() {
  return localStorage.getItem('username');
}

function saveUserToStorage(username) {
  localStorage.setItem('username', username);
}

export function* getUserName() {
  const user = yield call(loadUser);
  if (!user) {
    yield put(showUserDialog());
    yield take(USER_DIALOG_SUBMIT);
    const dialogUser = yield select(makeSelectInputUserDialog());
    yield put({ type: USER_DIALOG_SET_LOADING });
    try {
      yield call(request, requestUrl, {
        method: 'POST',
        data: dialogUser,
      });
      saveUserToStorage(dialogUser);
      yield put(loadUserSuccess(dialogUser));
    } catch (e) {
      yield put(showErrorMessage('Saving failed.'));
      yield put({ type: LOAD_USER_FAIL });
    }
  } else {
    yield put(hideUserDialog(user));
  }
}

export default function* root() {
  yield takeEvery([LOAD_USER, LOAD_USER_FAIL], getUserName);
}
