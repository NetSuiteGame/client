/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  LOAD_USER,
  SHOW_USER_DIALOG,
  HIDE_USER_DIALOG,
  USER_DIALOG_INPUT_CHANGED,
  USER_DIALOG_SUBMIT,
  USER_DIALOG_SET_ERROR,
  LOAD_USER_SUCCESS,
  NOTIFICATIONS_REMOVE,
  NOTIFICATIONS_ADD_SUCCESS,
  NOTIFICATIONS_ADD_INFO,
  NOTIFICATIONS_ADD_WARNING,
  NOTIFICATIONS_ADD_ERROR,
} from './constants';

export function loadUser() {
  return {
    type: LOAD_USER,
  };
}

export function showUserDialog() {
  return {
    type: SHOW_USER_DIALOG,
  };
}

export function hideUserDialog(username) {
  return {
    type: HIDE_USER_DIALOG,
    payload: {
      username,
    },
  };
}

export function userDialogInputChanged(value) {
  return {
    type: USER_DIALOG_INPUT_CHANGED,
    payload: {
      value,
    },
  };
}

export function userDialogSubmit() {
  return {
    type: USER_DIALOG_SUBMIT,
  };
}

export function userDialogSetError(message) {
  return {
    type: USER_DIALOG_SET_ERROR,
    payload: {
      message,
    },
  };
}

export function userDialogClearError() {
  return {
    type: USER_DIALOG_SET_ERROR,
  };
}

export function loadUserSuccess(username) {
  return {
    type: LOAD_USER_SUCCESS,
    payload: {
      username,
    },
  };
}

// export function showErrorMessage(message) {
//   return {
//     type: SHOW_ERROR_MESSAGE,
//     payload: {
//       message,
//     },
//   };
// }

export function removeNotification(key) {
  return {
    type: NOTIFICATIONS_REMOVE,
    payload: {
      key,
    },
  };
}

export function showSuccessMessage(message) {
  return {
    type: NOTIFICATIONS_ADD_SUCCESS,
    payload: {
      message,
    },
  };
}

export function showInfoMessage(message) {
  return {
    type: NOTIFICATIONS_ADD_INFO,
    payload: {
      message,
    },
  };
}

export function showWarningMessage(message) {
  return {
    type: NOTIFICATIONS_ADD_WARNING,
    payload: {
      message,
    },
  };
}

export function showErrorMessage(message) {
  return {
    type: NOTIFICATIONS_ADD_ERROR,
    payload: {
      message,
    },
  };
}
