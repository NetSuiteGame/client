/**
*
* Login screen in case user is for the first time
*
*/

import Button from '../../components/Button';
import H2 from '../../components/H2';
import Input from '../../components/Input';
import LoadingIndicator from '../../components/LoadingIndicator';
import React, { PropTypes, Children } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectShowUserDialog,
  makeSelectInputUserDialog,
  makeSelectErrorUserDialog,
  makeSelectUserDialogLoading,
} from './selectors';
import { userDialogInputChanged, userDialogSubmit, userDialogSetError } from './actions';

const Overlay = styled.div`
  background-color: #ddd;
  height: 100vh;
  left: 0;
  position: absolute;
  top: 0;
  width: 100vw;
  z-index: 1;
`;

const Window = styled.div`
  margin: 0 auto;
  position: relative;
  text-align: center;
  top: 50%;
  transform: translateY(-50%);
  width: 600px;
`;

const Heading = styled(H2)`
  padding-bottom: 0.25em;
`;

const DialogInput = styled(Input)`
  text-align: center;
  border-bottom: 1px dotted ${(props) => props.error ? 'red' : '#999'};
`;

const DialogButton = styled(Button)`
  box-sizing: border-box;
  padding: 0;
  width: 110px;
`;

function UserDialog(props) {
  const {
    children,
    dispatchDialogSubmit,
    inputError,
    inputValue,
    loading,
    onChangeUsername,
    setInputError,
    showUserDialog,
  } = props;
  if (showUserDialog) {
    return (
      <Overlay>
        <Window>
          <Heading>Your name</Heading>
          <DialogInput type="text" onChange={onChangeUsername} value={inputValue} error={inputError} />
          {loading ? <LoadingIndicator /> : <DialogButton onClick={handleSubmit}>Save</DialogButton>}
        </Window>
      </Overlay>
    );
  }
  return Children.only(children);

  function handleSubmit(evt) {
    if (inputValue) {
      dispatchDialogSubmit(evt);
    } else {
      setInputError('Empty field');
    }
  }
}

UserDialog.propTypes = {
  children: PropTypes.node,
  dispatchDialogSubmit: PropTypes.func,
  inputError: PropTypes.string,
  inputValue: PropTypes.string,
  loading: PropTypes.bool,
  onChangeUsername: PropTypes.func,
  setInputError: PropTypes.func,
  showUserDialog: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  showUserDialog: makeSelectShowUserDialog(),
  inputValue: makeSelectInputUserDialog(),
  inputError: makeSelectErrorUserDialog(),
  loading: makeSelectUserDialogLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: (evt) => dispatch(userDialogInputChanged(evt.target.value)),
    dispatchDialogSubmit: (evt) => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(userDialogSubmit());
    },
    setInputError: (message) => dispatch(userDialogSetError(message)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDialog);
