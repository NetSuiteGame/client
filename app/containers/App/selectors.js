/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');

const makeSelectCurrentUser = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('currentUser')
);

const makeSelectShowUserDialog = () => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['userDialog', 'show'])
);

const makeSelectInputUserDialog = () => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['userDialog', 'input'])
);

const makeSelectErrorUserDialog = () => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['userDialog', 'error'])
);

const makeSelectUserDialogLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['userDialog', 'loading'])
);


const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

const makeSelectError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('error')
);

const makeSelectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

export {
  selectGlobal,
  makeSelectCurrentUser,
  makeSelectLoading,
  makeSelectError,
  makeSelectShowUserDialog,
  makeSelectInputUserDialog,
  makeSelectLocationState,
  makeSelectErrorUserDialog,
  makeSelectUserDialogLoading,
};
