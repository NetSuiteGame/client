/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

// import Header from 'components/Header';
import Helmet from 'react-helmet';
import UserDialog from './UserDialog';
import React, { PropTypes } from 'react';
import styled from 'styled-components';
import withProgressBar from 'components/ProgressBar';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { loadUser } from './actions';
import { makeSelectCurrentUser, makeSelectLoading, makeSelectError } from 'containers/App/selectors';
const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0;
  min-height: 100%;
  padding: 0;
`;

export class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    const { dispatchLoadUser, username } = props;
    if (!username) {
      dispatchLoadUser();
    }
  }

  render() {
    const { username, children } = this.props;
    return (
      <AppWrapper>
        <Helmet
          titleTemplate="%s - NetSuite Game"
          defaultTitle="NetSuite Game"
          meta={[
            { name: 'description', content: 'NetSuite Game' },
          ]}
        />
        <UserDialog>
          {username ? children : <div></div>}
        </UserDialog>
      </AppWrapper>
    );
  }
}

App.propTypes = {
  children: PropTypes.node,
  username: PropTypes.string,
  dispatchLoadUser: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  username: makeSelectCurrentUser(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatchLoadUser: () => dispatch(loadUser()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withProgressBar(App));
