/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOAD_USER = 'netsuitegame/App/LOAD_USER';
export const LOAD_USER_SUCCESS = 'netsuitegame/App/LOAD_USER_SUCCESS';
export const LOAD_USER_FAIL = 'netsuitegame/App/LOAD_USER_FAIL';
export const SHOW_USER_DIALOG = 'netsuitegame/App/SHOW_USER_DIALOG';
export const HIDE_USER_DIALOG = 'netsuitegame/App/HIDE_USER_DIALOG';
export const DIALOG_SUBMIT = 'netsuitegame/App/DIALOG_SUBMIT';
export const USER_DIALOG_INPUT_CHANGED = 'netsuitegame/App/USER_DIALOG_INPUT_CHANGED';
export const USER_DIALOG_SUBMIT = 'netsuitegame/App/USER_DIALOG_SUBMIT';
export const USER_DIALOG_SET_ERROR = 'netsuitegame/App/USER_DIALOG_SET_ERROR';
export const USER_DIALOG_CLEAR_ERROR = 'netsuitegame/App/USER_DIALOG_CLEAR_ERROR';
export const USER_DIALOG_SET_LOADING = 'netsuitegame/App/USER_DIALOG_SET_LOADING';
export const USER_DIALOG_STOP_LOADING = 'netsuitegame/App/USER_DIALOG_STOP_LOADING';
export const NOTIFICATIONS_REMOVE = 'netsuitegame/App/NOTIFICATIONS_REMOVE';
export const NOTIFICATIONS_ADD_SUCCESS = 'netsuitegame/App/NOTIFICATIONS_ADD_SUCCESS';
export const NOTIFICATIONS_ADD_INFO = 'netsuitegame/App/NOTIFICATIONS_ADD_INFO';
export const NOTIFICATIONS_ADD_WARNING = 'netsuitegame/App/NOTIFICATIONS_ADD_WARNING';
export const NOTIFICATIONS_ADD_ERROR = 'netsuitegame/App/NOTIFICATIONS_ADD_ERROR';
export const DEFAULT_LOCALE = 'en';
