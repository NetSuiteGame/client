/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS, Map } from 'immutable';
import shortid from 'shortid';
import {
  LOAD_USER,
  LOAD_USER_SUCCESS,
  SHOW_USER_DIALOG,
  HIDE_USER_DIALOG,
  USER_DIALOG_INPUT_CHANGED,
  USER_DIALOG_SET_ERROR,
  USER_DIALOG_CLEAR_ERROR,
  USER_DIALOG_SET_LOADING,
  USER_DIALOG_STOP_LOADING,
  LOAD_USER_FAIL,
  NOTIFICATIONS_ADD_SUCCESS,
  NOTIFICATIONS_ADD_ERROR,
  NOTIFICATIONS_REMOVE,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  currentUser: '',
  userDialog: {
    show: false,
    input: '',
    error: null,
    loading: false,
  },
  notifications: Map(),
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_USER:
      return state.set('loading', true);
    case LOAD_USER_SUCCESS:
      return state
        .set('currentUser', action.payload.username)
        .setIn(['userDialog', 'loading'], false)
        .setIn(['userDialog', 'show'], false)
        .setIn(['userDialog', 'error'], null);
    case SHOW_USER_DIALOG:
      return state
        .setIn(['userDialog', 'show'], true);
    case HIDE_USER_DIALOG:
      return state
        .set('currentUser', action.payload.username)
        .setIn(['userDialog', 'show'], false);
    case USER_DIALOG_INPUT_CHANGED:
      return state
        .setIn(['userDialog', 'input'], action.payload.value);
    case USER_DIALOG_SET_ERROR:
      return state
        .setIn(['userDialog', 'error'], action.payload.message);
    case LOAD_USER_FAIL:
    case USER_DIALOG_CLEAR_ERROR:
      return state
        .setIn(['userDialog', 'error'], null)
        .setIn(['userDialog', 'loading'], false);
    case USER_DIALOG_SET_LOADING:
      return state
        .setIn(['userDialog', 'loading'], true);
    case USER_DIALOG_STOP_LOADING:
      return state
        .setIn(['userDialog', 'loading'], false);
    case NOTIFICATIONS_REMOVE:
      return state.update('notifications', (map) => map.delete(action.payload.key));
    case NOTIFICATIONS_ADD_SUCCESS: {
      const hash = shortid.generate();
      return state.update('notifications', (map) => map.set(
        hash, {
          hash,
          message: action.payload.message,
          type: 'SUCCESS',
        }
      ));
    }
    case NOTIFICATIONS_ADD_ERROR: {
      const hash = shortid.generate();
      return state.update('notifications', (map) => map.set(
        hash, {
          hash,
          message: action.payload.message,
          type: 'ERROR',
        }
      ));
    }
    default:
      return state;
  }
}

export default appReducer;
