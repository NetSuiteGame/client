/*
 *
 * Engine
 *
 */

import Content from '../../components/Content';
import H1 from '../../components/H1';
import Helmet from 'react-helmet';
import React, { PropTypes } from 'react';
import makeSelectEngine from './selectors';
import { WorkshopMenu } from '../../components/Menu';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

export class Engine extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Content>
        <Helmet
          title="Engine"
          meta={[
            { name: 'description', content: 'Description of Engine' },
          ]}
        />
        <WorkshopMenu helpRoute={'/workshop/engine/help'} />
        <H1>Engine</H1>
      </Content>
    );
  }
}

Engine.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Engine: makeSelectEngine(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Engine);
