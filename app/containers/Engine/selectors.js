import { createSelector } from 'reselect';

/**
 * Direct selector to the engine state domain
 */
const selectEngineDomain = () => (state) => state.get('engine');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Engine
 */

const makeSelectEngine = () => createSelector(
  selectEngineDomain(),
  (substate) => substate.toJS()
);

export default makeSelectEngine;
export {
  selectEngineDomain,
};
